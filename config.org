#+PROPERTY: header-args :comments link

* emacs configuration
oh look another literate emacs configuration file written by some nerd
yeah well at least this nerd is doing something to try and keep their life on track (wait till you see the org mode config, y'know)

** configuration reload helper
~C-x C-l~ re-renders this file into ~config.el~, and then reloads the entire emacs configuration in place
pretty neat

#+BEGIN_SRC emacs-lisp
  (defun reload-config ()
    (interactive)
    (org-babel-load-file
    (expand-file-name "config.org"
                   user-emacs-directory))
    (load-file user-init-file)
    (princ "Configuration reloaded."))

  (global-set-key (kbd "C-x C-l") 'reload-config)
#+END_SRC

** run the emacs server
this lets us use emacsclient to launch new buffers via ~$EDITOR~
which is cool because it lets you do things like pop open an emacs buffer from your terminal when git wants you to edit a commit message or something

#+begin_src emacs-lisp
  (server-start)
#+end_src

** emacs & OS-specific tuning
*** tune specpdl and eval depth size
Bump them up so we avoid issues with some LSP providers:
   
#+BEGIN_SRC emacs-lisp
    (setq max-specpdl-size 500
	  max-lisp-eval-depth 1000)
#+END_SRC

*** set minimum native-comp warning level
This really makes things a lot quieter.

#+begin_src emacs-lisp
  (setq warning-minimum-level :error)
#+end_src

*** add some useful OS PATHs
add  /usr/local/bin and friends so we can find git and go, add cargo paths.

#+BEGIN_SRC emacs-lisp
  (setq exec-path (append exec-path '((format "%s/.cargo/bin" (getenv "HOME")) "/usr/local/bin" "/usr/bin")))
#+END_SRC

*** site-lisp constant
sets a constant so we can always find site-lisp, also sets the load path for mu4e.
mu4e needs this special handling, becaus the mu4e emacs package ships with the mu package so the versions can be tightly coupled.

#+begin_src emacs-lisp
  (defconst site-lisp-dir (cond
			   ((equal system-type 'gnu/linux) "/usr/share/emacs/site-lisp/")
			   (t (concat "/usr/local/emacs/site-lisp/"))))
  (defconst mu4e-load-dir (cond
			   ((equal system-type 'gnu/linux) "/usr/local/share/emacs/site-lisp/mu4e/")
			   (t (concat site-lisp-dir "/mu4e"))))
#+end_src

*** packages
Package installation is idempotent, using ~use-package~.

**** package repos
Add the GNU, ELPA, MELPA, MELPA-stable, Marmalade and Org repos.
    
#+BEGIN_SRC emacs-lisp
  (setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
			   ("elpa" . "http://tromey.com/elpa/")
			   ("melpa" . "http://melpa.org/packages/")
			   ("melpa-stable" . "http://stable.melpa.org/packages/")
			   ("org" . "http://orgmode.org/elpa/")))
#+END_SRC

**** set up use-package
Set up use-package for organising package configuration.

#+BEGIN_SRC emacs-lisp
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))
  (eval-and-compile
    (setq use-package-always-ensure t
	  use-package-expand-minimally t))
#+END_SRC

** security & authentication
*** auth-source-pass
Look for login details for services in [[https://www.passwordstore.org/][pass]].

#+begin_src emacs-lisp
  (setq auth-source-debug t)
  (auth-source-pass-enable)
#+end_src

** look 'n feel

*** fonts

Set separate variable pitch and fixed pitch faces.

#+begin_src emacs-lisp
  (set-face-attribute 'default nil :font "Sarasa Mono Slab J-12")
  (set-face-attribute 'fixed-pitch nil :font "Sarasa Mono Slab J-12")
  (set-face-attribute 'variable-pitch nil :font "Kiwi Maru-13")

  (dolist (face '(default fixed-pitch))
    (set-face-attribute `,face nil :font "Sarasa Mono Slab J-12"))
#+end_src

*** GUI tweaks

Disable GUI bits, base font will be set by variable.
Also enables global line mode to highlight the current line.
    
#+BEGIN_SRC emacs-lisp
  (menu-bar-mode -1) (tool-bar-mode -1) (scroll-bar-mode -1)
  (global-hl-line-mode 1)
#+END_SRC

*** parenthesis

Load rainbow-delimiters to make brackets and braces easier to grok

#+BEGIN_SRC emacs-lisp
  (use-package rainbow-delimiters
    :ensure t
    :hook (prog-mode . rainbow-delimiters-mode))
#+END_SRC

*** theme
Load in module themes, set theme to ~kanagawa-dragon~.

#+BEGIN_SRC emacs-lisp    
  (use-package kanagawa-themes
    :ensure t
    :config
    (load-theme 'kanagawa-dragon t))
#+END_SRC

*** mode line
What to show at the bottom of a buffer
Currently customises -
 - mode-line-position: how the position in the current buffer is displayed

#+begin_src emacs-lisp
  (setq mode-line-position
	'("[%l,%c]" (:eval (format "%3d%%" (/ (window-point) 0.01 (point-max)))) "%%"))
#+end_src

*** title line
What to show at the top (window frame) of buffers.

#+BEGIN_SRC emacs-lisp
  (setq frame-title-format
     (list (format "%s %%S: %%j " (system-name))
       '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))
#+END_SRC

*** move keybindings
Set up default keybindings for moving between windows within frame with shift+{up,down,left,right}

#+begin_src emacs-lisp
  (windmove-default-keybindings)  
#+end_src

** startup
*** scratch space
boot into org, with the Org scratch space loaded instead of welcome message.
    
#+BEGIN_SRC emacs-lisp
  (setq inhibit-startup-screen t)
  (setq inhibit-splash-screen t)
  (setq initial-scratch-message nil)
#+END_SRC

** global keybindings
   Some binds and unbinds that I find useful.

#+BEGIN_SRC emacs-lisp
  ;; working this out was really upsetting, but i finally realised hitting C-z by accident was what was locking up emacs ;_;
  (global-unset-key (kbd "C-z"))
#+END_SRC

** global org & mu4e hotkeys
this sets up global hotkeys for capture and the agenda

#+begin_src emacs-lisp
  (global-set-key (kbd "C-c l") #'org-store-link)
  (global-set-key (kbd "C-c a") #'org-agenda)
  (global-set-key (kbd "C-c c") #'org-capture)
  (global-set-key (kbd "C-c m") #'mu4e)
#+end_src


** major mode to extension mapping
   High level additional config for major modes to use.
   Here we map filetypes to major modes, for example.

#+BEGIN_SRC emacs-lisp
  (add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.mdown\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.sh\\'" . shell-script-mode))
  (add-to-list 'auto-mode-alist '("\\.ledger\\'" . hledger-mode))
  (add-to-list 'auto-mode-alist '("\\.adoc\\'" . adoc-mode))
  (add-to-list 'auto-mode-alist '("\\.asciidoc\\'" . adoc-mode))
  (add-to-list 'auto-mode-alist '(".*/journal/[0-9]*$" . org-mode))
#+END_SRC

** utf-8
because it's not 1980 anymore babes

#+begin_src emacs-lisp
  (prefer-coding-system       'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (setq default-buffer-file-coding-system 'utf-8)  
#+end_src

** disable backup files
   yeah, yeah. I'm sure I'll regret this.
   update: i have regretted this several times, but also can't deal with the clutter of backup files, so here we are
   
#+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil) ; stop creating backup~ files
  (setq auto-save-default nil) ; stop creating #autosave# files  
#+END_SRC

** white space
ugh, white space.

#+BEGIN_SRC emacs-lisp
  (use-package ws-butler
    :ensure t
    :hook (prog-mode . ws-butler-mode))
#+END_SRC

** org mode
configuration for Org Mode, which I use, a lot

*** git sync helper for org directory
this takes the value of ~org-directory~ and does a safe of all org buffers, a git commit, a fetch & rebase, and then a push.
i use this to sync my org files between computers. an idle time (5 minutes) is also set up to run this.
#+begin_src emacs-lisp
    (defun org-git-sync ()
      (interactive)
      (message (format "Shadowing current directory %s with org dir %s..." default-directory org-directory))
      (setq prev-default-dir default-directory)         ; back up default directory
      (cd org-directory)                                ; set current directory to org directory
      (message "Saving all org buffers...")
      (org-save-all-org-buffers)                        ; call org-save-all-buffers to make sure everything is on disk
      (message "Pulling org files from remote...")
      (call-process "git" nil nil nil "fetch" "origin")     ; fetch upstream changes
      (message "Staging and committing files...")
      (call-process "git" nil nil nil "add" ".")        ; stage changes
      (call-process "git" nil nil nil "commit" "-a" "-m" "sync from emacs hotkey") ; commit changes
      (message "Pushing changes to remote...")
      (call-process "git" nil nil nil "rebase" "origin/main")    ; rebase current commits
      (call-process "git" nil nil nil "push" "origin" "main")
      (cd prev-default-dir)                       ; restore working directory
      (message "Org git sync done."))

  (run-with-idle-timer 360 t 'org-git-sync) ; run every 5 minutes when emacs is idle
#+end_src

*** org bullets
they look nicer
also set the bullet to an ankh because ~goff~

#+begin_src emacs-lisp
  (use-package org-bullets
    :ensure t
    :config
    (setq org-bullets-bullet-list '( "☥")))
#+end_src

*** general org config and agenda
this is the ~use-package~ section for org mode, which makes sure org is loaded before configuring it.
this also makes a few changes to clean up how bullets in lists are indented and to tidy text rendering.
it also will set a hook for ~prettify-symbols~, so things like source blocks will be a little prettier.

#+BEGIN_SRC emacs-lisp
  (use-package org
    :init
    ; needs to happen before org is loaded
    (setq
     org-modules '(org-habit))
    :hook
    ((org-mode . variable-pitch-mode)
     (org-mode . visual-line-mode)
     (org-mode . flyspell-mode)
     (org-mode . org-bullets-mode))
    :config
    (require 'org-agenda)
    (setq org-startup-indented t
          org-src-tab-acts-natively t
          org-startup-with-inline-images t
          org-hide-emphasis-markers t
          org-fontify-done-headline t
          org-enforce-todo-dependencies t
          org-hide-leading-stars t
          org-pretty-entities t
          org-use-fast-todo-selection 'expert          
          org-directory "~/org/"
          org-todo-keywords '((sequence "TODO(t!)" "INPROGRESS(i)" "NEXT(n)" "SOMEDAY(s)" "|" "DONE(d!)" "CANCELLED(c@)"))
          org-treat-insert-todo-headings-as-state-change t
          org-agenda-files '("~/org" "~/org/journal" "~/org/projects")
          org-agenda-sorting-strategy '(habit-up deadline-up category-keep)
          org-agenda-show-future-repeats nil
          org-agenda-window-setup 'current-window
          org-habit-show-habits t
          org-habit-show-in-agenda t
          org-habit-following-days 7
          org-habit-preceding-days 7
          org-habit-graph-column 70
          org-capture-templates
          '(("t" "TODO" entry (file+headline "~/org/unfiled.org" "Tasks")
             "* TODO %?\n  %i\n  %a")
            ("j" "Journal" entry (file+datetree get-journal-file-today)
             "* %?\nentered on %U\n  %i\n  %a"))
          org-default-notes-file '("~/org/unfiled.org"))
    :bind
    (:map org-agenda-mode-map
          ("g" . org-git-sync)))
#+END_SRC

*** org fonts
fixed fonts for certain elements like code blocks, tables, etc.

#+begin_src emacs-lisp
  (dolist (face '(org-code org-block org-table org-date org-link org-footnote))
  (set-face-attribute `,face nil :inherit 'fixed-pitch))
  (set-face-attribute 'org-code nil :inherit '(fixed-pitch shadow))
#+end_src

*** org journal
you know, for keeping a journal

#+begin_src emacs-lisp
  (defun get-journal-file-today ()
    "Return filename for today's journal entry."
    (let ((daily-name (format-time-string "%Y-%m-%d")))
      (expand-file-name (concat org-journal-dir daily-name ".org"))))

  (defun journal-file-today ()
    "Create and load a journal file based on today's date."
    (interactive)
    (find-file (get-journal-file-today)))

  (global-set-key (kbd "C-c f j") 'journal-file-today)

  (defun org-journal-file-header-func (time)
    "Custom function to create journal header."
    (concat
     (pcase org-journal-file-type
       ('daily "#+TITLE: daily journal\n#+STARTUP: showeverything")
       ('weekly "#+TITLE: weekly journal\n#+STARTUP: folded")
       ('monthly "#+TITLE: monthly journal\n#+STARTUP: folded")
       ('yearly "#+TITLE: yearly journal\n#+STARTUP: folded"))))

  (use-package org-journal
    :ensure t
    :config
    (setq org-journal-dir "~/org/journal/"
          org-journal-date-format "%Y-%m-%d.org"
          org-journal-file-header 'org-journal-file-header-func))
#+end_src

*** org babel languages
enable evaluation of org mode code blocks in emacs for specific languages (python, shell)

#+BEGIN_SRC emacs-lisp
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (shell . t)))
#+END_SRC

*** org mermaid
this helps with rendering ~mermaid~ source blocks in ~org~ documents, these are inserted as inline images in the ~org~ document.

#+BEGIN_SRC emacs-lisp
    (use-package ob-mermaid
      :config
      (org-babel-do-load-languages
      'org-babel-load-languages
      '((mermaid . t)
	(scheme . t)))
    )
    (add-hook 'org-babel-after-execute-hook
	      (lambda ()
		(org-redisplay-inline-images)))
#+END_SRC

** development
development/coding related things

*** git
set up magit for magic git things.

#+BEGIN_SRC emacs-lisp
(use-package magit
  :bind (("C-x g" . magit-status)
         ("C-x C-g" . magit-status)))
#+END_SRC	 

*** projectile
projectile helps me deal with project folders
    
#+BEGIN_SRC emacs-lisp
  (use-package projectile
    :ensure t
    :pin melpa-stable
    :init
    (projectile-mode +1)
    :bind (:map projectile-mode-map
		("s-p" . projectile-command-map)
		("C-c p" . projectile-command-map)))
#+END_SRC

*** go mode
enable ~go~ mode for working with Golang

#+begin_src emacs-lisp
  (use-package go-mode
    :ensure t)
#+end_src

*** rust (rust-mode)
I use this rather than ~rustic~ for rust development help (syntax checking, lint, coupled with LSP)

#+begin_src emacs-lisp
    (use-package rust-mode
      :ensure t)
#+end_src

*** LSP (lsp-mode)
this configures language server support for syntax highlighting and code formatting.
I tried eglot, but inlay hints were not supported for Rust which is a deal breaker, as it's incredibly useful to get those in your editor before compiling.
this does a bunch, it hooks all of the programming modes I use and have servers installed for, into ~lsp-mode~, sets paths to those servers.
I've also found I needed to tweak the file watch limits so I wasn't hitting them constantly, and enabling modeline diagnostics just helps to keep track of how many errors or what kind of feedback from the LSP server exists for the current buffer.
I also use ~C-c l~ as my LSP mode key prefix which I find pretty easy to remember.
I also set up a hook to enable ~lsp-format-buffer~ and ~lsp-organize-imports~ on saving files using ~lsp-mode~ so that any automatic fixes & import organisation supported by the language server are applied on save.

#+BEGIN_SRC emacs-lisp
  (use-package lsp-mode
    :ensure t
    :hook ((css-mode . lsp)
           (css-mode . lsp-install-save-hooks)
           (go-mode . lsp)
           (go-mode . lsp-install-save-hooks)
           (ruby-mode . lsp)
           (ruby-mode . lsp-install-save-hooks)
           (rust-mode . lsp)
           (rust-mode . lsp-install-save-hooks)
           (rust-mode . lsp)
           (rust-mode . lsp-install-save-hooks)
           (python-mode . lsp)
           (python-mode . lsp-install-save-hooks)
           (c-mode . lsp)
           (lsp-mode . lsp-enable-which-key-integration))

    :commands (lsp lsp-deferred lsp-format-buffer lsp-organize-imports)
    :bind
    (("C-c C-c r" . lsp-restart-workspace))
    :init
    (setq lsp-keymap-prefix "C-c l"
          lsp-log-io t
          lsp-modeline-diagnostics-enable t
          lsp-file-watch-threshold nil
          lsp-enable-file-watchers t
          lsp-print-performance nil
          lsp-idle-delay 0.6
          lsp-eldoc-render-all t
          lsp-rust-analyzer-server-command (list (replace-regexp-in-string "\n$" "" (shell-command-to-string (format "%s/.cargo/bin/rustup which rust-analyzer" (getenv "HOME")))))
          lsp-pylsp-server-command "~/.pyenv/versions/emacs39/bin/pylsp")
    :config
    (lsp-register-custom-settings
     '(("gopls.completeUnimported" t t)
       ("gopls.staticcheck" t t)
       ("gopls.matcher" "CaseSensitive"))))
    (defun lsp-install-save-hooks ()
      (add-hook 'before-save-hook #'lsp-format-buffer t t)
      (add-hook 'before-save-hook #'lsp-organize-imports t t))

  (use-package lsp-ui :commands lsp-ui-mode)

#+END_SRC

*** flycheck
flycheck, for syntax checking with ~lsp-mode~.

#+begin_src emacs-lisp
  (use-package flycheck
    :ensure t
    :bind
    (("C-c C-c l" . flycheck-list-errors)
     ("C-c C-c n" . flycheck-next-error)
     ("C-c C-c p" . flycheck-previous-error)
     ("C-c C-c 1" . flycheck-first-error)))
#+end_src

*** company
company is what I use to handle code completion with ~lsp-mode~

#+BEGIN_SRC emacs-lisp
  (use-package company
    :ensure t
    :custom
    (company-idle-delay 0.5) ;; how long to wait until popup
    ;; (company-begin-commands nil) ;; uncomment to disable popup
    :bind
    (:map company-active-map
	  ("C-n". company-select-next)
	  ("C-p". company-select-previous)
	  ("M-<". company-select-first)
	  ("M->". company-select-last))
    :config
    (setq
     company-minimum-prefix-length 1
     company-idle-delay 0.0
     company-tooltip-align-annotations t
     tab-always-indent 'complete)

    (defun check-expansion ()
      (save-excursion
      (if (looking-at "\\_>") t
	(backward-char 1)
      (if (looking-at "\\.") t
	(backward-char 1)
      (if (looking-at "::") t nil))))))

#+END_SRC

*** yaml
I use this to help with YAML formatting

#+BEGIN_SRC emacs-lisp
  (use-package yaml-mode
    :ensure t
    :init
    (add-hook 'yaml-mode-hook
	(lambda ()
		(define-key yaml-mode-map "\C-m" 'newline-and-indent))))
#+END_SRC

*** direnv support
this allows projects with a ~.envrc~ to influence state/environment for projects in Emacs
this is useful for picking up project environments for things like LSP finding the right packages and interpreters

#+begin_src emacs-lisp
  (use-package direnv
     :config
     (direnv-mode))
#+end_src

** communications
various things i do in emacs which i probably should be using something else for and just letting emacs be an editor
but i can't help myself

*** calendars
I use this to sync my CalDav calendar to an ~org-mode~ file so I can see calendar entries in emacs using Org.
I also use ~khal~ and ~vdirsyncer~ to sync CalDav and CardDav locally, using ~khalel~ as an emacs frontend, to create, schedule and modify CalDav & CardDav entries.

#+begin_src emacs-lisp
  (use-package org-caldav
    :ensure t
    :config
    (setq org-caldav-url (auth-source-pass-get 'secret "mail/junkyard.systems/caldav-url"))
    (setq org-caldav-calendar-id "Default")
    (setq org-caldav-inbox "~/org/personal-caldav.org")
    (setq org-caldav-files '("~/org/personal-caldav.org")))
  (use-package khalel
    :ensure t)
#+end_src

*** mail (sending)
I use emacs for sending mail too. This configures ~msmtp~ which lets me use profiles. I currently have this hard-coded to my personal account.

#+begin_src emacs-lisp
  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  (setq sendmail-program "/usr/bin/msmtp")
  (setq message-sendmail-extra-arguments '("-a" "junkyard.systems"))
#+end_src


